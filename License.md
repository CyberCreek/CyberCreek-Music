# License for CyberCreek Music

- The credits should not be changed.
- The bot-code can be used for **private hosting** and **personal usage**.
- Using the code for public usage is allowed if no profit is being made.

> **Note:** if you are found to be violating any of the above stated rule you might be asked to takedown your bot, happy
> listening!! Incase of any doubts in the license contact owner.